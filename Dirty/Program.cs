﻿using DirtyLogic;
using DirtyLogic.components;
using ECSTrial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dirty
{
    class Program
    {
        static void Main(string[] args)
        {
            var gm = new GameManager();
            var entry = gm.entry;
            entry.OnShowMessage += (s) => {
                Console.WriteLine("");
                Console.WriteLine(s);
                //Console.WriteLine("");
            };
            entry.ShowOptions_External = (option) =>
            {
                var ts = option.Texts;
                int op =1;
                foreach (var t in ts)
                {
                    Console.WriteLine("[{0}] {1}", op, t);
                    op++;
                }
                Console.WriteLine("");
                var key = Console.ReadKey(false);
                Console.WriteLine("");
                int value = key.KeyChar - '1';
                return value;
            };
            entry.OnWaitForAnyInput += () =>
            {
                Console.WriteLine("");
                Console.WriteLine("(Press any key to proceed)");
                Console.WriteLine("");
                Console.ReadKey(true);
            };
            gm.Initialize();
            
            

        }
    }
}
