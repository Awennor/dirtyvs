﻿using DirtyLogic;
using DirtyLogic.components;
using ECSTrial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dirty
{
    class ProgramOld
    {
        static void Main2(string[] args)
        {
            var gm = new GameManager();
            gm.Initialize();
            var accessor = gm.Accessor;
            var city = accessor.GetMyCityInfo();
            bool run = true;
            bool choosingEmployee = true;
            while (run)
            {
                var key = Console.ReadKey(true);
                if (key.KeyChar == 'c')
                {
                    Console.WriteLine(city.GoldTotal);
                }
                if (key.KeyChar == 't')
                {
                    Console.WriteLine("Time Passed");
                    accessor.WeekPass();
                }
                if (key.KeyChar == 'o')
                {
                    accessor.showEmployed();
                }
                if (key.KeyChar == 'p')
                {
                    Console.WriteLine("People for hire: ");
                    var es = accessor.GetPossibleEmployees();
                    for (int i = 0; i < es.Count; i++)
                    {
                        var e = es[i];
                        var sh = e.GetComponent<AttributeHolder>();
                        var sls = sh.AttributeLevels;
                        Console.WriteLine(i);
                        foreach (var sl in sls)
                        {
                            Console.WriteLine(" {0} {1}", sl.AttributeID, sl.Level);
                        }

                    }
                    key = Console.ReadKey(true);
                    int hired = key.KeyChar - '0';
                    accessor.hire(es[hired]);
                    Console.WriteLine("Hired {0}", hired);
                }
                if (key.KeyChar == 'x')
                {
                    run = false;
                }
            }

        }
    }
}
