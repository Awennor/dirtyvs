﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DirtyLogic.ExternalInteraction
{
    public class ExternalInteractionEntry
    {

        public event Action<OptionsExternalMenu> OnShowOptions;
        public event Action<string> OnShowMessage;
        public event Action OnWaitForAnyInput;
        private Func<OptionsExternalMenu, int> showOptions_External;

        public Func<OptionsExternalMenu, int> ShowOptions_External { get => showOptions_External; set => showOptions_External = value; }

        internal int ShowOptions(OptionsExternalMenu options)
        {

            if (OnShowOptions != null)
            {
                OnShowOptions(options);
            }

            return showOptions_External(options);
        }

        internal void WaitForAnyInput()
        {
            if (OnWaitForAnyInput != null)
            {
                OnWaitForAnyInput();
            }
        }

        internal void ShowMessage(string v)
        {
            if (OnShowMessage != null)
                OnShowMessage(v);
        }
    }
}
