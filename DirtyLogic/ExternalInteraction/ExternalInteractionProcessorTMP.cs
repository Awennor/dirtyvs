﻿using DirtyLogic.components;
using DirtyLogic.ExternalInteraction;
using ECSTrial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DirtyLogic
{
    class ExternalInteractionProcessorTMP : Processor
    {
        private ComponentSelector employees;
        
        OptionsExternalMenu options = new OptionsExternalMenu();
        private ExternalInteractionEntry entry;
        private Time time;
        private Entity player;
        List<MenuOptionData> mainMenuAdditionalOptions = new List<MenuOptionData>();

        internal List<MenuOptionData> MainMenuAdditionalOptions { get => mainMenuAdditionalOptions; set => mainMenuAdditionalOptions = value; }

        public ExternalInteractionProcessorTMP(ExternalInteractionEntry entry, Time time, Entity player)
        {
            this.entry = entry;
            this.time = time;
            this.player = player;

            employees = AddComponentSelector<Employee>();
            
        }


        public void ShowMainMenu()
        {
            entry.ShowMessage("What next?");
            options.Clear();
            options.Add("Advance time", AdvanceTime);
            options.Add("City status", ShowCityStatus);
            options.Add("Hire personnel", ShowHireMenu);
            options.Add("Show personnel", ShowPersonnel);
            for (int i = 0; i < MainMenuAdditionalOptions.Count; i++)
            {
                var mo = MainMenuAdditionalOptions[i];
                if(mo.IsAvailable())
                    options.Add(mo.Text, mo.ChosenMethod);
            }
            int option = entry.ShowOptions(options);
            options.Actions[option]();

        }

        private void ShowCityStatus()
        {
            int cityId = player.GetComponent<Mayor>().CityId;
            var city = new Entity(cityId).GetComponent<City>();
            entry.ShowMessage(
                string.Format(
                    "City Status\nPopulation: {0}\nGold: {1}\n", 
                    city.CityStats.Population,
                    city.GoldTotal
                    ));
            //entry.WaitForAnyInput();
            ShowMainMenu();
        }

        internal void ShowMenu(MenuData menuData)
        {
            entry.ShowMessage(menuData.Text);
            this.options.Clear();
            var ops = menuData.MenuOptionDatas;
            for (int i = 0; i < ops.Count; i++)
            {
                var mo = ops[i];
                if (mo.IsAvailable())
                    this.options.Add(mo.Text, mo.ChosenMethod);
            }
            int option = entry.ShowOptions(this.options);
            this.options.Actions[option]();

        }

        private void AdvanceTime()
        {
            time.WeekPass();
            ShowMainMenu();
        }

        private void ShowPersonnel()
        {
            entry.ShowMessage("Currently hired personnel: ");
            var list1 = employees.Entities;
            for (int i = 0; i < list1.Count; i++)
            {
                var e = list1[i];
                var ep = e.GetComponent<Employee>();
                if (ep.Employer == player.Id)
                {
                    entry.ShowMessage(string.Format("Employee ({0})", e.Id));
                }
            }
            entry.WaitForAnyInput();
            ShowMainMenu();
        }

        private void ShowHireMenu()
        {
            entry.ShowMessage("Who will you hire?");
            //options.ChosenCallback = HireChoice;
            options.Clear();
            var es = employees.Entities;
            //Console.WriteLine("DEBUG"+es.Count);
            for (int i = 0; i < es.Count; i++)
            {
                var e = es[i];
                var sh = e.GetComponent<AttributeHolder>();
                var sls = sh.AttributeLevels;
                
                foreach (var sl in sls)
                {
                    
                    options.Add(string.Format(" {0} {1}", sl.AttributeID, sl.Level));
                }
                
            }
            int op = entry.ShowOptions(options);
            if (op >= 0) {
                entry.ShowMessage("Hired "+(op+1));
                es[op].GetComponent<Employee>().Employer = player.Id;
                player.GetComponent<Employer>().Employees.Add(es[op].Id);
            }
            
            ShowMainMenu();
        }
    }
}
