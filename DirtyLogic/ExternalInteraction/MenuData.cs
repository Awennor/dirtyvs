﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DirtyLogic.ExternalInteraction
{
    class MenuData
    {
        string text;
        List<MenuOptionData> menuOptionDatas = new List<MenuOptionData>();
        
        public string Text { get => text; set => text = value; }
        internal List<MenuOptionData> MenuOptionDatas { get => menuOptionDatas; set => menuOptionDatas = value; }
    }
}
