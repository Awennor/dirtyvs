﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DirtyLogic.ExternalInteraction
{
    class MenuOptionData
    {
        Func<bool> isAvailable;
        Action chosenMethod;
        string text;

        public MenuOptionData(Func<bool> isAvailable, Action chosenMethod, string text)
        {
            this.isAvailable = isAvailable;
            this.chosenMethod = chosenMethod;
            this.text = text;
        }

        public Func<bool> IsAvailable { get => isAvailable; set => isAvailable = value; }
        public Action ChosenMethod { get => chosenMethod; set => chosenMethod = value; }
        public string Text { get => text; set => text = value; }
    }
}
