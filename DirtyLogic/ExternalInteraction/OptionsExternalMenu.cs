﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DirtyLogic.ExternalInteraction
{
    public class OptionsExternalMenu
    {
        List<String> texts = new List<string>();
        List<Action> actions = new List<Action>();
        Action<int> chosenCallback;

        public Action<int> ChosenCallback { get => chosenCallback; set => chosenCallback = value; }
        public List<string> Texts { get => texts; set => texts = value; }
        public List<Action> Actions { get => actions; set => actions = value; }

        internal void Clear()
        {
            Texts.Clear();
            Actions.Clear();
            ChosenCallback = null;
        }

        internal void Add(string v, Action weekPass)
        {
            Texts.Add(v);
            Actions.Add(weekPass);
        }

        internal void Add(string v)
        {
            Texts.Add(v);
        }
    }
}
