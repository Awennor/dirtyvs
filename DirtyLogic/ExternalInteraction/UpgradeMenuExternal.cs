﻿using DirtyLogic.components;
using DirtyLogic.processors;
using ECSTrial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DirtyLogic.ExternalInteraction
{
    class UpgradeMenuExternal : Processor
    {
        MenuOptionData menuOptionData;
        public ExternalInteractionProcessorTMP interactionProcessor;
        private Entity player;

        public UpgradeMenuExternal(Entity player, CityUpgradeSystem upgradeProcessor, ExternalInteractionProcessorTMP interaction)
        {
            this.interactionProcessor = interaction;
            this.upgradeProc = upgradeProcessor;
            this.player = player;
            MenuOptionData = new MenuOptionData(CanUpgrade, OnShowMenu, "Upgrade City");
            interactionProcessor.MainMenuAdditionalOptions.Add(menuOptionData);
            
        }

        internal MenuOptionData MenuOptionData { get => menuOptionData; set => menuOptionData = value; }
        MenuData menuData;
        private CityUpgradeSystem upgradeProc;

        private bool CanUpgrade()
        {
            return true;
        }

        private void OnShowMenu()
        {
            var city = new Entity(player.GetComponent<Mayor>().CityId);
            if (menuData == null)
            {
                menuData = new MenuData();
                var es = upgradeProc.GetUpgradesAvailable(new List<CityUpgradeData>(),city);
                for (int i = 0; i < es.Count; i++)
                {
                    var e = es[i];
                    var upgrade = e;
                    var text = upgrade.SkillId;
                    Func<bool> isAvailable = () =>
                    {
                        return true;
                    };
                    Action chosenMethod = () => {
                        upgradeProc.DoUpgrade(e, city);
                    };
                    menuData.MenuOptionDatas.Add(new MenuOptionData(isAvailable, chosenMethod, text));
                }
            }
            interactionProcessor.ShowMenu(menuData);
            interactionProcessor.ShowMainMenu();

        }
    }
}
