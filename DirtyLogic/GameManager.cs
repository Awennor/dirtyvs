﻿using DirtyLogic.components;
using DirtyLogic.ExternalInteraction;
using DirtyLogic.processors;
using ECSTrial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DirtyLogic
{
    public class GameManager
    {

        ExternalAccessor accessor = new ExternalAccessor();
        private AttributeHolder skillHolder;

        public ExternalAccessor Accessor { get => accessor; set => accessor = value; }
        public ExternalInteractionEntry entry { get; private set; } = new ExternalInteractionEntry();

        public void Initialize() {
            ECSMaster ecs = ECSMaster.Instance;

            var time = ecs.NewEntity();
            var timeComp = time.AddComponent<Time>();

            var pm = ecs.ProcessorManager;
            pm.RegisterProcessor(new MaxPopulationCapBySkillLevelProcessor(timeComp));
            CityProcessor cityProcessor = new CityProcessor();
            pm.RegisterProcessor(cityProcessor);
            TimeProcessor timeProcessor = new TimeProcessor();
            pm.RegisterProcessor(timeProcessor);
            var cityUpgrade = pm.RegisterProcessor<CityUpgradeSystem>();

            var personGenerator = pm.RegisterProcessor<PersonGenerator>();

            
            Accessor.weekPass = timeComp.WeekPass;
            var city = ecs.NewEntity();
            var cityC = city.AddComponent<City>();
            
            city.AddComponent<CityEffectHolder>();
            skillHolder = city.AddComponent<AttributeHolder>();
            cityC.GoldPerWeek = 20;
            cityC.GoldTotal = 100;
            CityStats cityStats = cityC.CityStats;
            cityStats.Population = 200;
            cityStats.Housing = cityStats.Population / CityProcessor.POPULATION_PER_HOUSING;
            cityC.CityStats = cityStats;
            Accessor.GetMyCityInfo = () =>
            {
                return cityC;
            };

            ecs.NewEntity().AddComponent<AttributeData>().AttributeId = "poison";   
            
            var player = ecs.NewEntity();
            var employer = player.AddComponent<Employer>();
            player.AddComponent<Mayor>().CityId = city.Id;
            employer.MaxHire = 3;

            for (int i = 0; i < 3; i++)
            {
                personGenerator.GeneratePerson();
            }

            new HousingInstaller();

            ExternalInteractionProcessorTMP externalInteractionProcessorTMP = new ExternalInteractionProcessorTMP(entry, timeComp, player);
            pm.RegisterProcessor(externalInteractionProcessorTMP);

            accessor.hire = (e) =>
            {
                e.GetComponent<Employee>().Employer = player.Id;
                employer.Employees.Add(e.Id);
            };

            accessor.showEmployed = () =>
            {
                List<Type> list = new List<Type>();
                list.Add(typeof(Employee));
                List<Entity> list1 = new List<Entity>();
                ECSMaster.Instance.FillWithEntitiesThatContainComponents(list1, list);
                for (int i = 0; i < list1.Count; i++)
                {
                    var e = list1[i];
                    var ep = e.GetComponent<Employee>();
                    if (ep.Employer == player.Id) {
                        Console.WriteLine("Employee ({0})", e.Id);
                    }
                }
            };
            new UpgradeMenuExternal(player, cityUpgrade, externalInteractionProcessorTMP);
            externalInteractionProcessorTMP.ShowMainMenu();

        }

        public class ExternalAccessor
        {
            internal Action weekPass;
            public Action showEmployed;
            public Func<City> GetMyCityInfo;
            public Action<Entity> hire;

            public void WeekPass() {
                weekPass();
            }

            public List<Entity> GetPossibleEmployees() {
                List<Type> list = new List<Type>();
                list.Add(typeof(Employee));
                List<Entity> list1 = new List<Entity>();
                ECSMaster.Instance.FillWithEntitiesThatContainComponents(list1, list);
                return list1;
            }
        }
    }

    
}
