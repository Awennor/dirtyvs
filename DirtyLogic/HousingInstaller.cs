﻿using DirtyLogic.components;
using ECSTrial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DirtyLogic
{
    class HousingInstaller
    {
        private const string HOUSING_SKILLID = "housing";

        internal HousingInstaller() {
            var e = ECSMaster.Instance.NewEntity();
            var sd = e.AddComponent<AttributeData>();
            sd.AttributeId = HOUSING_SKILLID;
            var cityUpgradeData = e.AddComponent<CityUpgradeData>();
            cityUpgradeData.MinCost = 10;
            cityUpgradeData.CostPerLevel = 50;
            cityUpgradeData.SkillId = HOUSING_SKILLID;
            e.AddComponent(new MaxPopulationCapBySkillLevel(200, 50, HOUSING_SKILLID));
        }
    }
}
