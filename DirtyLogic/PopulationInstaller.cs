﻿using DirtyLogic.components;
using ECSTrial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DirtyLogic
{
    class PopulationInstaller
    {
        public const string POPULATION_SKILLID = "population";
        public const string MAXPOPULATION_SKILLID = "maxpopulation";

        internal PopulationInstaller()
        {
            var e = ECSMaster.Instance.NewEntity();
            var sd = e.AddComponent<AttributeData>();
            sd.AttributeId = POPULATION_SKILLID;
            ECSMaster.Instance.NewEntity().AddComponent(new AttributeData(MAXPOPULATION_SKILLID));
            string[] bla = { };
            Func<List<int>, int> method = (values) => {
                return (int) ((values[1] - values[0]) * 0.5f + values[0]);
            };
            e.AddComponent(new AttributeFormula(
                new string[]{POPULATION_SKILLID, MAXPOPULATION_SKILLID},
                method
            ));

        }
    }
}
