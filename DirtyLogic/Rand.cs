﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DirtyLogic
{
    class Rand
    {

        static Rand rand = new Rand();

        static Random rnd;

        public Rand()
        {
            rnd = new Random();
        }

        public Rand(int seed)
        {
            rnd = new Random(seed);
        }

        internal static Rand Random { get => rand; set => rand = value; }

        public int Range(int min, int max) {
            var d = rnd.NextDouble();
            double d2 = (d * (max - min) + min);
            //Console.Write("RANDOM NUMBER "+d2);
            return (int) Math.Floor(d2);
        }
    }
}
