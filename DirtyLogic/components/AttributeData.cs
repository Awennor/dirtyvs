﻿using ECSTrial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DirtyLogic.components
{
    public class AttributeData : Component
    {
        string attributeId;

        public AttributeData(string attributeId)
        {
            this.attributeId = attributeId;
        }

        public AttributeData()
        {
            
        }

        public string AttributeId { get => attributeId; set => attributeId = value; }

        
    }
}
