﻿using ECSTrial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DirtyLogic.components
{
    class AttributeFormula : Component
    {
        string[] AttributeIds;
        Func<List<int>, int> formula;

        List<int> AttributeValues = new List<int>();


        public AttributeFormula(string[] attributeIds, Func<List<int>, int> formula)
        {
            AttributeIds = attributeIds;
            this.formula = formula;
        }

        public Func<List<int>, int> Formula { get => formula; set => formula = value; }
    }
}
