﻿using ECSTrial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DirtyLogic.components
{
    public class AttributeHolder : Component
    {
        List<AttributeLevel> attributeLevels = new List<AttributeLevel>();

        public List<AttributeLevel> AttributeLevels { get => attributeLevels; set => attributeLevels = value; }

        public class AttributeLevel {
            string id;
            int level;

            public AttributeLevel(string id, int level)
            {
                this.id = id;
                this.level = level;
            }

            public int Level { get => level; set => level = value; }
            public string AttributeID { get => id; set => id = value; }
        }

        internal void Set(string id, int level)
        {
            AttributeLevel skillLevel = GetAttributeLevel(id);
            if (skillLevel != null)
                skillLevel.Level = level;
            else
                attributeLevels.Add(new AttributeLevel(id, level));
        }

        private AttributeLevel GetAttributeLevel(string id)
        {
            AttributeLevel skillLevel = null;
            foreach (var sl in attributeLevels)
            {
                if (sl.AttributeID == id)
                {
                    skillLevel = sl;
                    break;
                }
            }

            return skillLevel;
        }

        internal int Get(string skillId)
        {
            var level = GetAttributeLevel(skillId);
            if (level != null) {
                return level.Level;
            }
            return 0;
        }

        private AttributeLevel GetOrCreateAttrLevel(string skillId)
        {
            var sl = GetAttributeLevel(skillId);
            if (sl == null) {
                sl = new AttributeLevel(skillId, 0);
                attributeLevels.Add(sl);
            }
            return sl;
        }

        internal void AddToSkillLevel(string skillId, int amountToAdd)
        {
            GetOrCreateAttrLevel(skillId).Level += amountToAdd;
        }

        
    }
}
