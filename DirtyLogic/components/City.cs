﻿using ECSTrial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DirtyLogic
{
    public class City : Component
    {
        int mayorId = -1;
        int goldTotal;
        int goldPerWeek;
        int foodPerWeek;

        CityStats cityStats;
        CityStats cityStatsPreviousWeek;
        

        public int GoldTotal { get => goldTotal; set => goldTotal = value; }
        public int GoldPerWeek { get => goldPerWeek; set => goldPerWeek = value; }
        public int FoodPerWeek { get => foodPerWeek; set => foodPerWeek = value; }
        public CityStats CityStats { get => cityStats; set => cityStats = value; }
        public CityStats CityStatsPreviousWeek { get => cityStatsPreviousWeek; set => cityStatsPreviousWeek = value; }
        public int MayorID { get => mayorId; set => mayorId = value; }
    }

    public struct CityStats {
        int population;
        int housing;

        public int Population { get => population; set => population = value; }
        public int Housing { get => housing; set => housing = value; }
    }
}
