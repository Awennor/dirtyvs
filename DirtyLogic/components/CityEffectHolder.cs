﻿using ECSTrial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DirtyLogic.components
{
    class CityEffectHolder : Component
    {

        List<CityEffect> cityEffects = new List<CityEffect>();

        internal List<CityEffect> CityEffects { get => cityEffects; set => cityEffects = value; }

        public class CityEffect {
            float deathRatePerWeek;

            public float DeathRatePerWeek { get => deathRatePerWeek; set => deathRatePerWeek = value; }
        }
    }
}
