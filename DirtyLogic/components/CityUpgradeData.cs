﻿using ECSTrial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DirtyLogic.components
{
    class CityUpgradeData : Component
    {
        string skillId;
        int minCost;
        int costPerLevel;

        List<AvailabilityRequisiteSkill> requisiteSkill = new List<AvailabilityRequisiteSkill>();

        public string SkillId { get => skillId; set => skillId = value; }
        public int MinCost { get => minCost; set => minCost = value; }
        public int CostPerLevel { get => costPerLevel; set => costPerLevel = value; }
        internal List<AvailabilityRequisiteSkill> RequisiteSkill { get => requisiteSkill; set => requisiteSkill = value; }
    }

    struct AvailabilityRequisiteSkill {
        readonly string skillId;
        readonly int minLevel;

        public AvailabilityRequisiteSkill(string skillId, int minLevel)
        {
            this.skillId = skillId;
            this.minLevel = minLevel;
        }
    };
}
