﻿using ECSTrial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DirtyLogic
{
    class Time : Component
    {
        public event Action OnWeekPass;
        public void WeekPass() {
            if (OnWeekPass != null) {
                OnWeekPass();
            }
                
        }
    }
}
