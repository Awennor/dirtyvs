﻿using DirtyLogic.components;
using ECSTrial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DirtyLogic
{
    class CityProcessor : Processor
    {
        public const int POPULATION_PER_HOUSING = 5;
        private ComponentSelector citySelector;

        public CityProcessor() {
            citySelector = base.AddComponentSelector(typeof(City), typeof(CityEffectHolder));
            base.AddComponentSelector_Callback<Time>(TimeSelector_OnUnitAdded);
        }

        private void TimeSelector_OnUnitAdded(Time obj)
        {
            obj.OnWeekPass += CityProcessor_OnWeekPass;
        }

        private void CityProcessor_OnWeekPass()
        {
            var entities = citySelector.Entities;
            
            foreach (var e in entities)
            {
                
                var city = e.GetComponent<City>();
                city.CityStatsPreviousWeek = city.CityStats;
                city.GoldTotal += city.GoldPerWeek;
                var efh = e.GetComponent<CityEffectHolder>();
                var efs = efh.CityEffects;
                var cityS = city.CityStats;

                int pop = cityS.Population;
                int housing = cityS.Housing;
                int maxPop = housing * POPULATION_PER_HOUSING;
                if (pop != maxPop) {
                    pop += (int) Math.Ceiling((maxPop - pop) * 0.4f);
                }


                for (int i = 0; i < efs.Count; i++)
                {
                    var effect = efs[i];
                    float deathRate = effect.DeathRatePerWeek;
                    var population = cityS.Population;
                    cityS.Population = population - (int) (population * deathRate);
                }
                city.CityStats = cityS;
            }
        }

    }
}
