﻿using DirtyLogic.components;
using ECSTrial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DirtyLogic.processors
{
    class CityUpgradeSystem : Processor
    {
        private ComponentSelector cityUpgrade;

        public CityUpgradeSystem() {
            cityUpgrade = AddComponentSelector<CityUpgradeData>();
        }

        public List<CityUpgradeData> GetUpgradesAvailable(List<CityUpgradeData> list, Entity city) {
            var es=cityUpgrade.Entities;
            foreach (var e in es)
            {
                list.Add(e.GetComponent<CityUpgradeData>());
            }
            return list;
        }

        public void DoUpgrade(CityUpgradeData upgrade, Entity city)
        {
            city.GetComponent<AttributeHolder>().AddToSkillLevel(upgrade.SkillId, 1);
        }


    }
}
