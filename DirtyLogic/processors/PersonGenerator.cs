﻿using DirtyLogic.components;
using ECSTrial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DirtyLogic.processors
{
    class PersonGenerator : Processor
    {
        private ComponentSelector skillDatas;

        public PersonGenerator() {
            skillDatas = AddComponentSelector<AttributeData>();
        }

        public Entity GeneratePerson() {
            var e = ECSMaster.Instance.NewEntity();
            e.AddComponent<Employee>();
            var sH = e.AddComponent<AttributeHolder>();
            var sEs = skillDatas.Entities;
            foreach (var s in sEs)
            {
                var sd = s.GetComponent<AttributeData>();
                var id = sd.AttributeId;
                int level = Rand.Random.Range(1, 5);
                sH.Set(id, level);
            }
            return e;
        }
        

    }
}
