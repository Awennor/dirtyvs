﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ECSTrial
{
    public class ComponentSelectorSingle<T> : ComponentSelector where T:Component
    {
        List<T> holdedComponents = new List<T>();
        public ComponentSelectorSingle() {
            AddComponentsWished<T>();
            OnUnitAdded += ComponentSelectorSingle_OnUnitAdded;
        }

        private void ComponentSelectorSingle_OnUnitAdded(Entity obj)
        {
            holdedComponents.Add(obj.GetComponent<T>());
        }

        
    }

    public static class ComponentSelectorSingleExtension {
        public static ComponentSelectorSingle<T> AddComponentSelectorSingle<T>(this Processor e) where T : Component
        {
            ComponentSelectorSingle<T> componentSelectorSingle = new ComponentSelectorSingle<T>();
            e.AddComponentSelector(componentSelectorSingle);
            return componentSelectorSingle;
        }
    }

    
}
