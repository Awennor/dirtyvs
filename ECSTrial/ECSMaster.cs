﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ECSTrial
{

    public class ECSMaster
    {

        static ECSMaster instance = new ECSMaster();
        ProcessorManager processorManager = new ProcessorManager();

        Dictionary<Type, List<Component>> compHolder = new Dictionary<Type, List<Component>>();
        static int highestEntityId;

        public static ECSMaster Instance
        {
            get => instance;

            set => instance = value;
        }
        public ProcessorManager ProcessorManager { get => processorManager; set => processorManager = value; }

        public Entity NewEntity()
        {
            highestEntityId++;
            var e = new Entity(highestEntityId);
            return e;
        }

        

        public void FillWithEntitiesThatContainComponents(List<Entity> aux, List<Type> componentsWished)
        {

            for (int i = 0; i <= highestEntityId; i++)
            {
                bool add = EntityContainComponent(componentsWished, i);
                if (add)
                {
                    aux.Add(new Entity(i));
                }

            }
        }

        internal bool EntityContainComponent(List<Type> componentsWished, int entityId)
        {
            bool contain = true;
            for (int j = 0; j < componentsWished.Count; j++)
            {
                var comp = componentsWished[j];
                var list = GetList(comp);

                if (list.Count <= entityId)
                {
                    contain = false;
                    break;
                }
                if (list[entityId] == null)
                {
                    contain = false;
                    break;
                }
            }

            return contain;
        }

        internal void AddComponent(Entity e, Component c)
        {
            var type = c.GetType();
            List<Component> comps = GetList(type);
            while (comps.Count <= e.Id)
            {
                comps.Add(null);
            }
            comps[e.Id] = c;
            //Console.WriteLine("Adding component {0}",c.GetType().Name);
            processorManager.EntityChanged(e);
            
        }

        internal T AddComponent<T>(Entity e) where T : Component, new()
        {

            T c = new T();
            AddComponent(e, c);
            return c;
        }

        private List<Component> GetList(Type type)
        {
            List<Component> comps;
            if (!compHolder.ContainsKey(type))
                compHolder.Add(type, new List<Component>());
            comps = compHolder[type];
            return comps;
        }

        internal T GetComponent<T>(Entity e) where T : Component
        {
            var type = typeof(T);
            List<Component> comps = GetList(type);
            if (comps.Count <= e.Id)
            {
                return null;
            }
            return (T)comps[e.Id];
        }
    }

    public static class EntityComponentAccessors
    {
        public static T GetComponent<T>(this Entity e) where T : Component
        {
            return ECSMaster.Instance.GetComponent<T>(e);
        }

        public static T AddComponent<T>(this Entity e) where T : Component, new()
        {
            return ECSMaster.Instance.AddComponent<T>(e);
        }

        public static void AddComponent(this Entity e, Component c)
        {
            ECSMaster.Instance.AddComponent(e, c);
        }
    }

    public class Component
    {
        public Component()
        {
        }
    }
}
