﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ECSTrial
{
    public struct Entity
    {
        int id;

        public Entity(int id)
        {
            this.id = id;
        }

        public int Id { get => id; }
    }
}
