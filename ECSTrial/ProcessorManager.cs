﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
//using System.Linq;
using System.Text;

namespace ECSTrial
{
    public class ProcessorManager
    {
        List<ComponentSelector> registeredSelectors = new List<ComponentSelector>();
        List<Processor> registeredProcessors = new List<Processor>();
        List<SuperProcessor> registeredSuperProcessors = new List<SuperProcessor>();
        List<Entity> aux = new List<Entity>();

        public event Action<Processor> OnProcessorRegistered;

        internal ProcessorManager()
        {

        }

        public T RegisterProcessor<T>() where T : Processor, new()
        {
            var t = new T();
            RegisterProcessor(t);
            return t;
        }

        public void RegisterProcessor(Processor p)
        {
            

            var ss = p.Selectors;
            AddSelectors(ss);
            if (p is SuperProcessor)
            {
                var sp = p as SuperProcessor;
                registeredSuperProcessors.Add(sp);
                foreach (var rp in registeredProcessors)
                {
                    sp.ProcessorAware(rp);
                }
            }
            registeredProcessors.Add(p);
            foreach (var sp in registeredSuperProcessors)
            {
                sp.ProcessorAware(p);
            }
        }

        private void AddSelectors(List<ComponentSelector> ss)
        {
            
            foreach (var item in ss)
            {
                AddSelector(item);
            }

        }

        public void AddSelector(ComponentSelector item)
        {
            var cm = ECSMaster.Instance;
            aux.Clear();
            //Console.WriteLine("Adding selector");
            cm.FillWithEntitiesThatContainComponents(aux, item.ComponentsWished);
            //Console.WriteLine("Entities on selector added "+aux.Count);
            item.AddUnits(aux);
            registeredSelectors.Add(item);
        }

        internal void EntityChanged(Entity e)
        {
            var cm = ECSMaster.Instance;
            //Console.WriteLine("Entity changed Process Manager");
            foreach (var s in registeredSelectors)
            {
                if (cm.EntityContainComponent(s.ComponentsWished, e.Id))
                {
                    s.AddEntity(e);
                }
                else
                {
                    s.RemoveEntity(e);
                }
            }
        }
    }

    public class SuperProcessor : Processor
    {
        public virtual void ProcessorAware(Processor p)
        {

        }
    }

    public class Processor
    {

        private List<ComponentSelector> selectors = new List<ComponentSelector>();

        public List<ComponentSelector> Selectors { get => selectors; }

        public ComponentSelector AddComponentSelector(Type type)
        {
            ComponentSelector processorSelector = AddComponentSelector();
            processorSelector.AddComponentsWished(type);
            return processorSelector;
        }

        public ComponentSelector AddComponentSelector(ComponentSelector selector)
        {
            selectors.Add(selector);
            return selector;
        }

        public ComponentSelector AddComponentSelector(Type type, Type type2)
        {
            ComponentSelector processorSelector = AddComponentSelector();
            processorSelector.AddComponentsWished(type);
            processorSelector.AddComponentsWished(type2);
            return processorSelector;
        }

        public ComponentSelector AddComponentSelector<T>()
        {
            return AddComponentSelector(typeof(T));
        }

        public ComponentSelector AddComponentSelector_Callback<T>(Action<T> ComponentCallback) where T : Component
        {
            ComponentSelector processorSelector = AddComponentSelector(typeof(T));
            processorSelector.OnUnitAdded += (e) =>
            {
                ComponentCallback(e.GetComponent<T>());
            };
            return processorSelector;
        }

        private ComponentSelector AddComponentSelector()
        {
            ComponentSelector processorSelector = new ComponentSelector();
            selectors.Add(processorSelector);
            return processorSelector;
        }

        public void AddComponentsWished(Type type)
        {
            if (selectors.Count == 0)
            {
                selectors.Add(new ComponentSelector());
            }
            selectors[0].AddComponentsWished(type);
        }

    }

    public class ComponentSelector
    {
        private List<Type> componentsWished = new List<Type>();
        public List<Type> ComponentsWished { get => componentsWished; set => componentsWished = value; }
        public ReadOnlyCollection<Entity> Entities { get => containedEntities_ReadOnly; }

        ReadOnlyCollection<Entity> containedEntities_ReadOnly;
        public event Action<Entity> OnUnitAdded;
        List<Entity> containedEntities = new List<Entity>();

        internal ComponentSelector()
        {
            containedEntities_ReadOnly = containedEntities.AsReadOnly();
        }

        public void AddComponentsWished(Type type)
        {
            componentsWished.Add(type);
        }

        internal void AddUnits(List<Entity> aux)
        {
            foreach (var e in aux)
            {
                AddEntity(e);
            }

        }

        internal void AddEntity(Entity e)
        {
            if (containedEntities.Contains(e)) return;
            containedEntities.Add(e);
            if (OnUnitAdded != null)
            {
                OnUnitAdded(e);
            }
        }

        internal void RemoveEntity(Entity e)
        {
            containedEntities.Remove(e);
        }

        public ComponentSelector AddComponentsWished<T>()
        {
            AddComponentsWished(typeof(T));
            return this;
        }
    }
}
